// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer 
//data containing only the car years and log the result in the console as it was returned.

const inventory = require("./test/inventory");

const result = inventory => {
    if(  inventory === undefined  || inventory.constructor !== Array){
        const arr = [];
        return arr ;
    }
    let car_years = [];
    for(let index = 0; index < inventory.length; index++) {
        car_years.push(inventory[index].car_year);
    }
    return car_years;
}
module.exports = result;