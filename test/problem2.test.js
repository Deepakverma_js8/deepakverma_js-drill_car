const problem2 = require('../problem2') ;

const  data = require('./inventory');

test('testing problem2', () => {
    expect(problem2(data)).toBe('Last car is a Lincoln Town Car')
});
