const problem1 = require('../problem1');
const  data = require('./inventory');

test('Testing problem1', () => {
    expect(problem1(data,33)).toBe('Car 33 is a 2011 Jeep Wrangler')
});
// we will run this by npm test 
// And it will tell whether test passed or not . 
//output = Car 33 is a 2011 Jeep Wrangler
