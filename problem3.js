// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const result = inventory => {
    if(  inventory === undefined  || inventory.constructor !== Array){
        const arr = [];
        return arr ;
    }
    let length = inventory.length;
    for(let i=0;i< length;i++){
        for(let j = i+1;j< length;j++){
            if(inventory[j].car_model.toUpperCase() < inventory[i].car_model.toUpperCase()){
                let temp = inventory[i];
                inventory[i] = inventory[j];
                inventory[j] = temp;

            }
        }
    }
    return inventory;
}
module.exports = result;